from Video_count import *
from tkinter import *
from tkinter import filedialog
# from multiprocessing import Queue, Process
import multiprocessing
from Connection import *


class App:
    def open_file(self):
        self.filename = filedialog.askopenfilename(filetypes=(("video", "*.avi;*.mp4;*.mov")
                                                              , ("All files", "*.*")))
        if self.filename != '':
            self.count_right = 0
            self.count_left = 0
            self.current_situation.set('Видео загружено')
            self.q.put(self.filename)

    def saveDatabase(self):
        try:
            conFirst = HooksConnection()
            conFirst.createConections()
            conFirst.insertValues('"NSILAEVA"."Hooks"', self.count_right, self.count_left)
        except Exception as e:
            print(e, 'Error')

    def setText(self, word):
        self.count_left.set(word[0])
        self.count_right.set(word[1])

    def create(self, toplevel):
        lable_video = Label(toplevel, width=15, textvariable=self.current_situation, font="Arial 15",
                            bg="lightyellow")
        lable_video.pack(fill=BOTH)
        left_rear = Label(toplevel, text="Левый ряд", font="Arial 15",
                          bg="lightyellow")
        left_rear.place(x=0, y=90)
        right_rear = Label(toplevel, text="Правый ряд", font="Arial 15",
                           bg="lightyellow")
        self.lable_rear_left = Label(toplevel, width=15, bd=4, bg="lightyellow", font="Arial 12")
        self.lable_rear_left.pack(side=LEFT)
        right_rear.place(x=230, y=90)
        self.lable_rear_right = Label(toplevel, width=15, bd=4, bg="lightyellow",
                                      font="Arial 12")
        self.lable_rear_right.pack(side=RIGHT)
        b = Button(toplevel, bg="gray", width=25, height=4, text="Выбрать файл", command=self.open_file,
                   font="Arial 13")
        b.place(x=69, y=295)
        database = Button(toplevel, bg="gray", width=25, height=4, text="Сохранить в БД", command=self.saveDatabase,
                          font="Arial 13")
        database.place(x=69, y=400)
        self.current_situation.set('Загрузите видео')
        self.update_label_L()
        self.update_lable_R()

    def __init__(self, toplevel, q, q1, q2, ev):
        self.q = q
        self.q1 = q1
        self.q2 = q2
        self.ev = ev
        # self.video = Video(r'')
        self.count = 0
        self.count_left = 0
        self.count_right = 0
        self.current_situation = StringVar()
        toplevel.update_idletasks()
        toplevel.title("Centered")
        w = toplevel.winfo_screenwidth()
        h = toplevel.winfo_screenheight()
        self.size = (350, 500)
        x = w // 2 - self.size[0] // 2
        y = h // 2 - self.size[1] // 2
        toplevel.geometry("{}x{}+{}+{}".format(self.size[0], self.size[1], w - (self.size[0] + self.size[0] // 10), y))
        self.filename = None
        self.create(toplevel)

    def update_label_L(self):

        self.lable_rear_right.configure(text='count: {}'.format(self.count_left))
        self.lable_rear_right.after(10, self.update_label_L)  # call this method again in 1,000 milliseconds
        # self.lable_rear_right.config(text='count: {}'.format(self.count_right))
        # self.lable_rear_right.after(10, self.update_label)
        if self.q1.empty() == False:
            self.count_left = self.q1.get()
        # print(self.ev.empty())
        if self.ev.empty() == False and self.ev.get() == False:
            self.current_situation.set('Загрузите видео')
            # if not self.ev.empty():
            #     self.current_situation.set('Видео закончено')
            # self.count_right = self.q1.get()
            # self.count +=1

    # print (self.count)
    def update_lable_R(self):
        self.lable_rear_left.configure(text='count: {}'.format(self.count_right))
        self.lable_rear_left.after(10, self.update_lable_R)  # call this method again in 1,000 milliseconds
        # self.lable_rear_right.config(text='count: {}'.format(self.count_right))
        # self.lable_rear_right.after(10, self.update_label)
        if self.q2.empty() == False:
            self.count_right = self.q2.get()
            # if not self.ev.empty():
            #     self.current_situation.set('Видео закончено')
            #     self.ev.get()
            # if self.ev.empty():
            #     self.current_situation.set('Видео загружено')


def started(q, q1, q2, ev):
    root = Tk()
    app = App(root, q, q1, q2, ev)
    root.resizable(width=False, height=False)
    root.mainloop()


def start_video(q, q1, q2, ev):
    while True:
        if q.empty != False:
            start(Video(q1, q2, q.get()), ev)


if __name__ == '__main__':
    # logger = multiprocessing.log_to_stderr()
    # logger.setLevel(multiprocessing.SUBDEBUG)
    flag = False
    q = multiprocessing.Queue(1)
    q1 = multiprocessing.Queue(1)
    q2 = multiprocessing.Queue(1)
    ev = multiprocessing.Queue(1)
    p = multiprocessing.Process(target=started, args=(q, q1, q2, ev))
    p.start()
    e = multiprocessing.Process(target=start_video, args=(q, q1, q2, ev))
    e.start()
    p.join()
    if not p.is_alive():
        e.terminate()
    e.join()

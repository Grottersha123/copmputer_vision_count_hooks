import pyhdb
import configparser
import os.path


class HooksConnection:
    def __init__(self):
        self.cursor = None
        self.connection = None
        config = configparser.ConfigParser()
        config.sections()
        if os.path.exists('Configurations.ini'):
            config.read('Configurations.ini')
            print('Your file is load')
        if not os.path.exists('Configurations.ini'):
            # Todo нужно создать файл Configurations
            config['DEFAULT'] = {
                'host': "localhost",
                'port': 0,
                'user': "UserDatabase",
                'password': "Yourpassword"
            }
            with open('Configurations.ini', 'w') as configfile:
                config.write(configfile)

        self.host = config['DEFAULT']['host']
        self.port = config['DEFAULT']['port']
        self.user = config['DEFAULT']['user']
        self.password = config['DEFAULT']['password']

    def createConections(self):
        self.connection = pyhdb.connect(
            host=self.host,
            port=self.port,
            user=self.user,
            password=self.password
        )
        try:
            self.cursor = self.connection.cursor()
            print('Connection is set')
        except Exception as e:
            print(e)
            self.connection = None

    def isConnect(self):
        return not self.connection is None

    def closeConnection(self):
        self.connection.close()

    def getLastId(self, nameTable):
        if self.isConnect():
            self.cursor.execute('Select * from {0}'.format(nameTable))
            res = self.cursor.fetchall()
            return int(res[len(res) - 1][0])

    def insertValues(self, nameTable, *args):
        if self.isConnect():
            try:
                print(','.join(list(map(str, args))))
                id = self.getLastId(nameTable) + 1
                self.cursor.execute(
                    "INSERT INTO {0} VALUES({1},{2})".format(nameTable, id, ','.join(list(map(str, args)))))
                self.connection.commit()
                self.connection.close()
                print('Your record is inserted')
            except Exception as Ex:
                print(Ex)
                # print(self.getLastId(nameTable))
        else:
            print("Isn't connect to your database")

    def setConfiguration(self, host, port, user, password):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        print('New configurations are set')


if __name__ == '__main__':
    conFirst = HooksConnection()
    conFirst.createConections()
    # conFirst.insertValues('"NSILAEVA"."Hooks"',5,25,16)
    res = conFirst.getLastId('"NSILAEVA"."Hooks"')
    print(res)
    # insertValues('"NSILAEVA"."Hooks"',4,25,16)

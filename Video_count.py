import cv2
import numpy as np
from collections import deque

def take_character(H, K, H_s, K_s):
    H_s1 = abs(K_s[0] - H_s[0])  # height
    K_s1 = abs(H_s[1] - K_s[1])  # wi
    if H[0] <= H_s1 <= H[1] and K_s1 >= K[0] and K_s1 <= K[1]:
        return True
    else:
        return False


def max_min(points):
    a = np.array(points)
    x = a[:, 0][:, 0]
    y = a[:, 0][:, 1]
    min_x = min(x)
    max_x = max(x)
    min_y = min(y)
    max_y = max(y)
    return (min_x, max_y), (max_x, min_y)


class Video:
    def __init__(self, q=None, q1=None, filename=''):
        blank_image = np.zeros((300, 200, 3), np.uint8)
        self.green = False
        self.yellow = False
        self.white = False
        self.q = q
        self.q1 = q1
        self.cap = cv2.VideoCapture(filename)
        self.h_min_yellow = np.array((0, 73, 80), np.uint8)  # мин желтый
        self.h_max_yellow = np.array((80, 255, 255), np.uint8)  # макс желтый
        self.h_min_green = np.array((79, 103, 72), np.uint8)
        self.h_max_green = np.array((117, 255, 255), np.uint8)
        self.img = None
        self.count_left = 0
        self.count_right = 0
        self.points_leftWhite = []
        self.points_rightWhite = []
        self.points_leftGreen = []
        self.points_rightGreen = []
        self.points_left = []
        self.points_right = []
        self.h = 640  # Размер по умолчанию
        self.w = 480
        self.pts = deque(maxlen=64)
        self.counter = 0
        self.center = (0,0)
        self.dX, self.dY = (0, 0)
        self.dirY,self.dirX = ("","")

    def __del__(self):
        self.cap.release()

    def getSizedFrame(self):
        fr, self.img = self.cap.read()
        self.img = cv2.resize(self.img, (self.h, self.w))

    def resize(self, h, w):
        if self.img is not None:
            self.img = cv2.resize(self.img, (h, w))

    def show_img(self, img=None):
        if img is None:
            cv2.imshow(self.getFrame())

    def cut_area(self, x, y, w, h):
        self.img = self.img[x:y, w, h]

    def getWidth(self):
        return np.size(self.img, 1)

    def getHeight(self):
        return np.size(self.img, 0)

    def right(self):
        return self.img[0:self.getHeight() // 10,
               (self.getWidth() // 2) + (self.getWidth() // 2) // 2:self.getWidth()]

    def left(self):
        return self.img[0:self.getHeight() // 10, 0:(self.getWidth() // 2) - (self.getWidth() // 2) // 2]

    def create_windows(self):
        cv2.namedWindow('original_video', cv2.WINDOW_NORMAL)
        cv2.namedWindow('counting_hooks', cv2.WINDOW_NORMAL)
        cv2.moveWindow('original_video', 0, 0)
        cv2.moveWindow('counting_hooks', 640, 0)
        blank_image = np.zeros((300, 200, 3), np.uint8)

    # Todo Сделать настройку по цвету по доминирующим
    # цветам, если будет тот который нам нужен то уже по этому флагу определять
    def color_detect(self):
        hsv = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)
        thresh_yellow = cv2.inRange(hsv, self.h_min_yellow, self.h_max_yellow)
        thresh_green = cv2.inRange(hsv, self.h_min_green, self.h_max_green)
        moments_y = cv2.moments(thresh_yellow, 1)
        moments_g = cv2.moments(thresh_green, 1)
        im2, contours, hierarchy = cv2.findContours(thresh_green, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:20]
        im2, contours_ol, hierarchy = cv2.findContours(thresh_yellow, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        contours_yel = sorted(contours_ol, key=cv2.contourArea, reverse=True)[:20]
        self.green = True if len(contours) >= len(contours_yel) else False
        self.yellow = True if len(contours) < len(contours_yel) else False

    def get_maskLeft(self, img, area, h_min, h_max, points=[]):
        y = 0
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        thresh = cv2.inRange(hsv, h_min, h_max)
        moments = cv2.moments(thresh, 1)
        dM01 = moments['m01']
        dM10 = moments['m10']
        dArea = moments['m00']
        if dArea > area:
            x = int(dM10 / dArea)
            y = int(dM01 / dArea)
            self.center = (x,y)
            cv2.circle(img, (x, y), 10, (0, 0, 255), -1)
            points.append(y)
        self.pts.appendleft(self.center)
        if self.count_left == 0 and len(points) == 2:
            self.count_left += 1
        if len(points) == 2:
            start = points[0]
            end = points[1]
            if end > start and not (end - start) in [1, 2, 3]:
                self.count_left += 1
                # print('{0} - count'.format(self.count_left))
            points.pop(0)
            self.q1.put(self.count_left)
        for i in np.arange(1, len(self.pts)):
            # if either of the tracked points are None, ignore
            # them
            if self.pts[i] == (0,0):
                self.pts.pop()
                continue

            # check to see if enough points have been accumulated in
            # the buffer
            # print(self.pts)

            if len(self.pts) >=3 and i == 1:
                # compute the difference between the x and y
                # coordinates and re-initialize the direction
                # text variables
                self.dX = self.pts[-2][0] - self.pts[i][0]
                self.dY = self.pts[-2][1] - self.pts[i][1]
                print(self.dY,self.dX)
                # ensure there is significant movement in the
                # x-direction
                if np.abs(self.dX) > 8:
                    self.dirX = "East" if np.sign(self.dX) == 1 else "West"

                # ensure there is significant movement in the
                # y-direction
                if np.abs(self.dY) > 8:
                    self.dirY = "North" if np.sign(self.dY) == 1 else "South"

                # handle when both directions are non-empty
                # print(self.dirY,self.dirX)
            thickness = int(np.sqrt(64 / float(i + 1)) * 2.5)
            print(self.pts[i-1],self.pts[i])
            cv2.line(img, self.pts[i - 1], self.pts[i], (0, 0, 255), thickness)

        return self.count_left

    def get_maskRight(self, img, area, h_min, h_max, points=[]):
        y = 0
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        thresh = cv2.inRange(hsv, h_min, h_max)
        moments = cv2.moments(thresh, 1)
        dM01 = moments['m01']
        dM10 = moments['m10']
        dArea = moments['m00']
        if dArea > area:
            x = int(dM10 / dArea)
            y = int(dM01 / dArea)
            cv2.circle(img, (x, y), 10, (0, 0, 255), -1)
            points.append(y)
        if self.count_right == 0 and len(points) == 2:
            self.count_right += 1
        if len(points) == 2:
            start = points[0]
            end = points[1]
            if end > start and not (end - start) in [1, 2, 3, 4, 5, 6]:
                self.count_right += 1
                # print('{0} - count'.format(self.count_left))
            points.pop(0)
            self.q.put(self.count_right)
        return self.count_right


def start(video, q):
    try:
        cv2.namedWindow('original_video', cv2.WINDOW_NORMAL)
        cv2.namedWindow('counting_hooks', cv2.WINDOW_NORMAL)
        cv2.moveWindow('original_video', 0, 0)
        cv2.moveWindow('counting_hooks', 640, 0)
        count_left = 0
        count_right = 0
        blank_image = np.zeros((300, 200, 3), np.uint8)
        i = 0  # Todo Переменная для первого кадра переделать
        j = 0  # Подсчет кадров
        while True:
            video.getSizedFrame()
            cv2.line(video.img, (0, 50), (640, 50), (255, 255, 0), 2)
            if i == 0:
                video.color_detect()
            if video.green == True:
                video.get_maskLeft(video.left(), 150, video.h_min_green, video.h_max_green,
                                   points=video.points_left)
                video.count_right = 0
                # count_right = video.get_mask_right_g(video.le)
            if video.yellow == True:
                # print('ok')
                if j >1:
                    # print('ok')
                    video.get_maskLeft(video.left(), 200, video.h_min_yellow, video.h_max_yellow,
                                       points=video.points_left)
                    video.get_maskRight(video.right(), 100, video.h_min_yellow, video.h_max_yellow,
                                        points=video.points_right)
                else:
                    # print('yeh')
                    j += 1
            x = 20
            y = 100
            font = cv2.FONT_HERSHEY_COMPLEX_SMALL
            font1 = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(blank_image, 'LEFT', (0, 100), font, 1, (255, 255, 255), 1, cv2.LINE_AA)
            cv2.putText(blank_image, '{0}'.format(video.count_left), (20, 150), font1, 1, (255, 255, 255), 2,
                        cv2.LINE_AA)
            cv2.line(blank_image, (100, 0), (100, 300), (123, 34, 23), 2)
            cv2.putText(blank_image, 'RIGHT', (115, 100), font, 1, (255, 255, 255), 1, cv2.LINE_AA)
            cv2.putText(blank_image, '{0}'.format(video.count_right), (130, 150), font1, 1, (255, 255, 255), 2,
                        cv2.LINE_AA)

            cv2.imshow('original_video', video.img)
            cv2.imshow('counting_hooks', blank_image)
            blank_image = np.zeros((300, 200, 3), np.uint8)
            k = cv2.waitKey(25) & 0xFF
            i += 1
            video.counter+=1
            if k == 27:
                video.__del__()
                cv2.destroyAllWindows()
                q.put(False)
                return count_left
    except Exception as e:
        print('End video',e)
        video.__del__()
        cv2.destroyAllWindows()
        q.put(False)
        return count_left


if __name__ == '__main__':
    video = Video(filename=r'D:\Git_project\copmputer_vision_count_hooks\IMG_0087 (online-video-cutter.com).avi')

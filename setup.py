from cx_Freeze import setup, Executable
import os
os.environ['TCL_LIBRARY'] = r'c:\Users\Grotter\Miniconda3\tcl\tcl8.6'
os.environ['TK_LIBRARY'] = r'C:\Users\Grotter\Miniconda3\tcl\tk8.6'
build_exe_options = {"packages": ["tkinter","cv2",'multiprocessing',"numpy"],"include_files":['tcl86t.dll', 'tk86t.dll']}

setup(
    name = "This_is_war",
    version = "0.1",
    description = "Sort Methods",
    options = {"build_exe": build_exe_options},
    executables = [Executable("Count_Gui_variant.py")]
    )